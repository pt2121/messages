package com.pt2121.talk

import android.os.Bundle
import android.widget.TextView
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.setPadding
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.pt2121.db.ChatDatabase
import com.pt2121.talk.util.DiskExecutor
import kotlinx.android.synthetic.main.activity_main.chatItemList

class ChatActivity : AppCompatActivity(), MessageAdapter.Listener {

    private val viewModel by lazy {
        val db = ChatDatabase.getInstance(this)
        ViewModelProviders.of(
            this,
            ChatViewModelFactory(
                db.messageDao(),
                db.attachmentDao(),
                DiskExecutor.instance
            )
        ).get(ChatViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val adapter = MessageAdapter(this)
        chatItemList.adapter = adapter
        val layoutManager = chatItemList.layoutManager as LinearLayoutManager

        chatItemList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val lastPosition = layoutManager.findLastVisibleItemPosition()
                if (lastPosition > adapter.itemCount - VISIBLE_THRESHOLD) {
                    viewModel.loadNextPage()
                }
            }
        })

        viewModel.getMessages()
            .observe(this, Observer { page ->
                adapter.setData(page)
            })

        if (savedInstanceState == null) {
            viewModel.loadNextPage()
        }
    }

    override fun onLongClickMessage(messageId: Long) {
        showBottomSheet(R.string.delete_message) {
            viewModel.deleteMessage(messageId)
        }
    }

    override fun onLongClickAttachment(attachmentId: String) {
        showBottomSheet(R.string.delete_attachment) {
            viewModel.deleteAttachment(attachmentId)
        }
    }

    private fun showBottomSheet(@StringRes title: Int, deleteAction: () -> Unit) {
        BottomSheetDialog(this).apply {
            val deleteTextView = TextView(context).apply {
                setText(title)
                setPadding(context.resources.getDimensionPixelSize(R.dimen.space_normal))
                setOnClickListener {
                    deleteAction.invoke()
                    dismiss()
                }
            }
            setContentView(deleteTextView)
        }.show()
    }

    companion object {
        const val VISIBLE_THRESHOLD = 4
    }
}

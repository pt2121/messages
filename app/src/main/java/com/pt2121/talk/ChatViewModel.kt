package com.pt2121.talk

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.pt2121.db.dao.AttachmentDao
import com.pt2121.db.dao.MessageDao
import com.pt2121.db.entity.ChatMessage
import com.pt2121.talk.model.ChatItem
import com.pt2121.talk.util.toChatItems
import io.reactivex.Single
import io.reactivex.disposables.SerialDisposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.Executor
import java.util.concurrent.atomic.AtomicBoolean

class ChatViewModel(
    private val messageDao: MessageDao,
    private val attachmentDao: AttachmentDao,
    private val dbExecutor: Executor
) : ViewModel() {

    private var isLoading = AtomicBoolean(false)
    private var page = 0

    private val chatMessages = MutableLiveData<List<ChatMessage>>()
    private val serialDisposable = SerialDisposable()

    private var cachedCount = -1

    fun deleteMessage(messageId: Long) {
        chatMessages.value?.let { messages ->
            chatMessages.value = messages.filterNot { it.message?.id == messageId }
        }

        dbExecutor.execute {
            messageDao.deleteMessageById(messageId)
            cachedCount = -1
        }
    }

    fun deleteAttachment(attachmentId: String) {
        chatMessages.value?.let { messages ->
            chatMessages.value = removeAttachmentFromList(messages.toMutableList(), attachmentId)
        }

        dbExecutor.execute {
            attachmentDao.deleteAttachmentById(attachmentId)
            cachedCount = -1
        }
    }

    fun getMessages(): LiveData<List<ChatItem>> =
        Transformations.map(chatMessages) { page ->
            page.flatMap { chatMessage ->
                chatMessage.toChatItems()
            }
        }

    fun loadNextPage() {
        if (isLoading.compareAndSet(false, true)) {
            serialDisposable.set(
                getMessageCount()
                    .doOnSuccess { cachedCount = it }
                    .flatMap { count -> queryNextPageIfHasMore(count) }
                    .subscribeOn(Schedulers.io())
                    .subscribe({ list ->
                        if (list.isNotEmpty()) {
                            page += 1
                            chatMessages.postValue(chatMessages.value?.plus(list) ?: list)
                        }
                        isLoading.set(false)
                    }) { error ->
                        Log.e(ChatViewModel::class.java.simpleName, "$error")
                        isLoading.set(false)
                    }
            )
        }
    }

    private fun queryNextPageIfHasMore(count: Int): Single<List<ChatMessage>> {
        val hasMore = count == 0 || count > page * PAGE_SIZE
        return if (hasMore) {
            val offset = page * PAGE_SIZE
            messageDao.getPagedMessages(offset, PAGE_SIZE)
        } else {
            Single.just(emptyList())
        }
    }

    private fun getMessageCount() = if (cachedCount == -1) {
        messageDao.getMessageCount()
    } else {
        Single.just(cachedCount)
    }

    private fun removeAttachmentFromList(
        messages: MutableList<ChatMessage>,
        attachmentId: String
    ): List<ChatMessage> {
        val index = messages.indexOfFirst { chatMessage ->
            chatMessage.attachments.any { it.id == attachmentId }
        }
        return if (index > -1) {
            val updatedMessage = messages[index]
            updatedMessage.attachments = updatedMessage.attachments.filterNot { it.id == attachmentId }
            messages[index] = updatedMessage
            messages
        } else {
            messages
        }
    }

    companion object {
        private const val PAGE_SIZE = 20
    }
}

class ChatViewModelFactory(
    private val messageDao: MessageDao,
    private val attachmentDao: AttachmentDao,
    private val dbExecutor: Executor
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return ChatViewModel(messageDao, attachmentDao, dbExecutor) as T
    }
}
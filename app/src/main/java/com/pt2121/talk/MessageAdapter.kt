package com.pt2121.talk

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.pt2121.talk.model.AttachmentItem
import com.pt2121.talk.model.ChatItem
import com.pt2121.talk.model.MessageItem
import com.pt2121.talk.util.DiffUtilAdapter
import com.pt2121.talk.viewholder.AttachmentViewHolder
import com.pt2121.talk.viewholder.MessageViewHolder
import com.pt2121.talk.viewholder.SelfMessageViewHolder
import kotlin.properties.Delegates

class MessageAdapter(private val listener: Listener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>(),
    DiffUtilAdapter {

    init {
        setHasStableIds(true)
    }

    private var items: List<ChatItem> by Delegates.observable(emptyList()) { _, old, new ->
        onChanged(old, new) { oldItem, newItem ->
            when {
                oldItem is MessageItem && newItem is MessageItem -> oldItem.adapterId == newItem.adapterId
                oldItem is AttachmentItem && newItem is AttachmentItem -> oldItem.adapterId == newItem.adapterId
                else -> false
            }
        }
    }

    fun setData(page: List<ChatItem>) {
        this.items = page
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return when (viewType) {
            R.layout.row_message -> MessageViewHolder(parent) { listener.onLongClickMessage(it.messageId) }
            R.layout.row_message_me -> SelfMessageViewHolder(parent) { listener.onLongClickMessage(it.messageId) }
            R.layout.row_attachment -> AttachmentViewHolder(parent) { listener.onLongClickAttachment(it.attachmentId) }
            else -> throw IllegalArgumentException("unknown view type $viewType")
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            R.layout.row_message -> (holder as MessageViewHolder).bindTo(items[position] as MessageItem)
            R.layout.row_message_me -> (holder as SelfMessageViewHolder).bindTo(items[position] as MessageItem)
            R.layout.row_attachment -> (holder as AttachmentViewHolder).bindTo(items[position] as AttachmentItem)
            else -> throw IllegalArgumentException("unknown view type")
        }
    }

    override fun getItemViewType(position: Int): Int {
        val item = items[position]
        return when (item) {
            is MessageItem -> if (item.isFromMe) R.layout.row_message_me else R.layout.row_message
            is AttachmentItem -> R.layout.row_attachment
        }
    }

    override fun getItemId(position: Int): Long {
        val item = items[position]
        return when (item) {
            is MessageItem -> item.adapterId
            is AttachmentItem -> item.adapterId
        }
    }

    override fun getItemCount(): Int = items.size

    interface Listener {
        fun onLongClickMessage(messageId: Long)
        fun onLongClickAttachment(attachmentId: String)
    }
}
package com.pt2121.talk.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

// UI-layer models for the adapter

sealed class ChatItem

@Parcelize
data class MessageItem(
    val adapterId: Long,
    val messageId: Long,
    val content: String,
    val userName: String,
    val userAvatarId: String,
    val isFromMe: Boolean
) : ChatItem(), Parcelable

@Parcelize
data class AttachmentItem(
    val adapterId: Long,
    val isFromMe: Boolean,
    val attachmentId: String,
    val title: String,
    val url: String
) : ChatItem(), Parcelable
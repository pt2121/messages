package com.pt2121.talk.util

import java.util.concurrent.Executor
import java.util.concurrent.Executors

object DiskExecutor {
    val instance: Executor by lazy {
        Executors.newSingleThreadExecutor()
    }
}
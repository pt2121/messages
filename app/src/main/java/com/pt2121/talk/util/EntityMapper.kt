package com.pt2121.talk.util

import com.pt2121.db.entity.Attachment
import com.pt2121.db.entity.ChatMessage
import com.pt2121.db.entity.Message
import com.pt2121.db.entity.User
import com.pt2121.talk.model.AttachmentItem
import com.pt2121.talk.model.ChatItem
import com.pt2121.talk.model.MessageItem

fun ChatMessage.toChatItems(): List<ChatItem> {
    val user = users.first()

    val messageItem: List<ChatItem> = message?.let {
        listOf(it.toMessageItem(user))
    } ?: emptyList()

    val attachmentItems: List<ChatItem> = attachments.map { attachment ->
        attachment.toAttachmentItem(user)
    }

    return messageItem + attachmentItems
}

private fun Message.toMessageItem(user: User): MessageItem =
    MessageItem(
        adapterId = id(id),
        messageId = id,
        content = content,
        userName = user.name,
        userAvatarId = user.avatarId,
        isFromMe = user.id == Constants.SELF_ID
    )

private fun Attachment.toAttachmentItem(user: User): AttachmentItem =
    AttachmentItem(
        adapterId = id(messageId, id),
        isFromMe = user.id == Constants.SELF_ID,
        attachmentId = id,
        title = title,
        url = url
    )
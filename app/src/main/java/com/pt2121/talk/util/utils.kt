package com.pt2121.talk.util

/**
 * https://github.com/airbnb/epoxy/blob/master/epoxy-adapter/src/main/java/com/airbnb/epoxy/EpoxyModel.java
 */
fun id(id1: Long, id2: String? = null): Long {
    val result = hashLong64Bit(id1)
    return 31 * result + hashString64Bit(id2)
}

/**
 * From http://stackoverflow.com/a/11554034
 */
fun hashLong64Bit(value: Long): Long {
    var long = value
    long = long xor (long shl 21)
    long = long xor long.ushr(35)
    long = long xor (long shl 4)
    return long
}

/**
 * From http://www.isthe.com/chongo/tech/comp/fnv/index.html#FNV-1a
 */
private fun hashString64Bit(str: CharSequence?): Long {
    val string = str ?: return 0

    var result = -0x340d631b7bdddcdbL
    val len = string.length
    for (i in 0 until len) {
        result = result xor string[i].toLong()
        result *= 0x100000001b3L
    }
    return result
}
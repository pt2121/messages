package com.pt2121.talk.viewholder

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.ViewGroup.MarginLayoutParams
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.CustomViewTarget
import com.bumptech.glide.request.transition.Transition
import com.pt2121.talk.R
import com.pt2121.talk.model.AttachmentItem

class AttachmentViewHolder(
    parent: ViewGroup,
    private val onLongClickAttachment: (AttachmentItem) -> Unit
) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context)
        .inflate(R.layout.row_attachment, parent, false)
) {

    private val bigMargin: Int by lazy {
        attachmentCardView.context.resources.getDimensionPixelSize(R.dimen.big_margin)
    }

    private val spaceNormal: Int by lazy {
        attachmentCardView.context.resources.getDimensionPixelSize(R.dimen.space_normal)
    }

    private val attachmentMargin: Int by lazy {
        attachmentCardView.context.resources.getDimensionPixelSize(R.dimen.attachment_margin)
    }

    private val attachmentText = itemView.findViewById<TextView>(R.id.attachmentTextView)
    private val attachmentCardView = itemView.findViewById<CardView>(R.id.attachmentCardView)

    private var attachmentItem: AttachmentItem? = null

    fun bindTo(attachmentItem: AttachmentItem) {
        this.attachmentItem = attachmentItem
        attachmentText.text = attachmentItem.title

        val glideOptions = RequestOptions().apply {
            centerCrop()
        }

        val leftMargin = getLeftMargin(attachmentItem)
        val rightMargin = getRightMargin(attachmentItem)

        attachmentCardView.setOnLongClickListener {
            onLongClickAttachment.invoke(attachmentItem)
            true
        }

        attachmentCardView.layoutParams = (attachmentCardView.layoutParams as MarginLayoutParams)
            .apply {
                setMargins(
                    leftMargin, topMargin,
                    rightMargin, bottomMargin
                )
            }

        Glide.with(attachmentCardView)
            .load(attachmentItem.url.replace("http://", "https://"))
            .apply(glideOptions)
            .into(object : CustomViewTarget<CardView, Drawable>(attachmentCardView) {
                override fun onLoadFailed(errorDrawable: Drawable?) {
                }

                override fun onResourceCleared(placeholder: Drawable?) {
                }

                override fun onResourceReady(resource: Drawable, transition: Transition<in Drawable>?) {
                    attachmentCardView.background = resource
                }
            })
    }

    private fun getRightMargin(attachmentItem: AttachmentItem): Int =
        if (attachmentItem.isFromMe)
            spaceNormal
        else
            attachmentMargin

    private fun getLeftMargin(attachmentItem: AttachmentItem): Int =
        if (attachmentItem.isFromMe)
            bigMargin
        else
            spaceNormal
}
package com.pt2121.talk.viewholder

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.pt2121.talk.R
import com.pt2121.talk.model.MessageItem

class MessageViewHolder(
    parent: ViewGroup,
    private val onLongClickMessage: (MessageItem) -> Unit
) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context)
        .inflate(R.layout.row_message, parent, false)
) {

    private val nameView = itemView.findViewById<TextView>(R.id.nameTextView)
    private val messageView = itemView.findViewById<TextView>(R.id.messageTextView)
    private val avatarImageView = itemView.findViewById<ImageView>(R.id.avatarImageView)

    private var messageItem: MessageItem? = null

    fun bindTo(messageItem: MessageItem) {
        this.messageItem = messageItem
        nameView.text = messageItem.userName
        messageView.text = messageItem.content

        messageView.setOnLongClickListener {
            onLongClickMessage.invoke(messageItem)
            true
        }

        Glide.with(avatarImageView)
            .load(messageItem.userAvatarId)
            .placeholder(R.drawable.ic_mood_black_24dp)
            .error(R.drawable.ic_mood_bad_black_24dp)
            .apply(RequestOptions.circleCropTransform())
            .into(avatarImageView)
    }
}
package com.pt2121.talk.viewholder

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.pt2121.talk.R.id
import com.pt2121.talk.R.layout
import com.pt2121.talk.model.MessageItem

class SelfMessageViewHolder(
    parent: ViewGroup,
    private val onLongClickMessage: (MessageItem) -> Unit
) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context)
        .inflate(layout.row_message_me, parent, false)
) {

    private val messageView = itemView.findViewById<TextView>(id.selfMessageTextView)

    private var messageItem: MessageItem? = null

    fun bindTo(messageItem: MessageItem) {
        this.messageItem = messageItem
        messageView.text = messageItem.content

        messageView.setOnLongClickListener {
            onLongClickMessage.invoke(messageItem)
            true
        }
    }
}
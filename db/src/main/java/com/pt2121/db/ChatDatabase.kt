package com.pt2121.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.pt2121.db.R.raw
import com.pt2121.db.dao.AttachmentDao
import com.pt2121.db.dao.MessageDao
import com.pt2121.db.dao.UserDao
import com.pt2121.db.entity.Attachment
import com.pt2121.db.entity.Message
import com.pt2121.db.entity.User
import com.pt2121.db.raw.Conversation
import java.util.concurrent.Executors

@Database(
    entities = [Attachment::class, Message::class, User::class],
    version = 1,
    exportSchema = true
)
abstract class ChatDatabase : RoomDatabase() {

    abstract fun messageDao(): MessageDao

    abstract fun attachmentDao(): AttachmentDao

    internal abstract fun userDao(): UserDao

    companion object {

        private const val DATABASE_NAME = "conversation-db"

        @Volatile
        private var instance: ChatDatabase? = null

        fun getInstance(context: Context): ChatDatabase {
            return instance ?: synchronized(this) {
                instance ?: buildDatabase(context).also { instance = it }
            }
        }

        private fun buildDatabase(
            context: Context
        ): ChatDatabase {
            return Room.databaseBuilder(
                context, ChatDatabase::class.java,
                DATABASE_NAME
            ).addCallback(object : Callback() {
                override fun onCreate(db: SupportSQLiteDatabase) {
                    super.onCreate(db)
                    Executors.newSingleThreadExecutor().execute {
                        val database = getInstance(context)
                        database.runInTransaction {
                            prepopulateData(context, database)
                        }
                    }
                }
            }).build()
        }

        private fun prepopulateData(context: Context, chatDatabase: ChatDatabase) {
            Utils.loadRawResSync<Conversation>(context, raw.data)
                ?.let { conversation ->
                    insertUsers(chatDatabase, conversation)
                    insertMessages(chatDatabase, conversation)
                    insertAttachments(chatDatabase, conversation)
                }
        }

        private fun insertAttachments(
            chatDatabase: ChatDatabase,
            conversation: Conversation
        ) {
            val attachments = conversation.messages
                .flatMap { message ->
                    message.attachments?.map {
                        Attachment(it.id, it.title, it.url, it.thumbnailUrl, message.id)
                    }.orEmpty()
                }

            chatDatabase.attachmentDao().insertAttachments(attachments)
        }

        private fun insertMessages(
            chatDatabase: ChatDatabase,
            conversation: Conversation
        ) {
            val messages = conversation.messages
                .map { message ->
                    Message(message.id, message.userId, message.content)
                }

            chatDatabase.messageDao().insertAll(messages)
        }

        private fun insertUsers(
            chatDatabase: ChatDatabase,
            conversation: Conversation
        ) {
            val users = conversation.users
                .map { user ->
                    User(user.id, user.name, user.avatarId)
                }

            chatDatabase.userDao().insertAll(users)
        }
    }
}
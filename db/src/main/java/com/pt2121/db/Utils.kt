package com.pt2121.db

import android.content.Context
import androidx.annotation.RawRes
import androidx.annotation.WorkerThread
import com.google.gson.Gson

object Utils {

    @WorkerThread
    inline fun <reified T> loadRawResSync(context: Context, @RawRes rawRes: Int): T? =
        try {
            context.resources
                .openRawResource(rawRes)
                .bufferedReader()
                .use { Gson().fromJson(it, T::class.java) }
        } catch (e: Exception) {
            null
        }
}

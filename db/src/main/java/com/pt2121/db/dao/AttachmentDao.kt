package com.pt2121.db.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.pt2121.db.entity.Attachment

@Dao
interface AttachmentDao {

    @Insert
    fun insertAttachments(attachments: List<Attachment>)

    @Delete
    fun deleteAttachment(attachment: Attachment)

    @Query("DELETE FROM attachments WHERE id = :attachmentId")
    fun deleteAttachmentById(attachmentId: String): Int
}
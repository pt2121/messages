package com.pt2121.db.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import com.pt2121.db.entity.ChatMessage
import com.pt2121.db.entity.Message
import io.reactivex.Single

@Dao
interface MessageDao {

    @Insert
    fun insertAll(messages: List<Message>)

    @Delete
    fun delete(message: Message)

    @Query("DELETE FROM messages WHERE id = :messageId")
    fun deleteMessageById(messageId: Long): Int

    @Transaction
    @Query("SELECT * FROM messages LIMIT :limit OFFSET :offset")
    fun getPagedMessages(offset: Int, limit: Int): Single<List<ChatMessage>>

    @Query("SELECT COUNT(id) FROM messages")
    fun getMessageCount(): Single<Int>
}
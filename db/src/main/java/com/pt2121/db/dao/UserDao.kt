package com.pt2121.db.dao

import androidx.room.Dao
import androidx.room.Insert
import com.pt2121.db.entity.User

@Dao
internal abstract class UserDao {

    @Insert
    internal abstract fun insertAll(users: List<User>)
}
package com.pt2121.db.entity

import androidx.room.Embedded
import androidx.room.Relation

class ChatMessage {

    @Embedded
    var message: Message? = null

    @Relation(
        parentColumn = "id",
        entityColumn = "message_id"
    )
    var attachments: List<Attachment> = emptyList()

    @Relation(
        parentColumn = "user_id",
        entityColumn = "id"
    )
    var users: Set<User> = emptySet()
}
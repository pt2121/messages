package com.pt2121.db.raw

/**
 * represents data.json
 */
internal data class Conversation(
    val messages: List<Message>,
    val users: List<User>
)
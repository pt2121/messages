package com.pt2121.db.raw

/**
 * represents Message in data.json
 */
internal data class Message(
    val id: Long,
    val userId: Long,
    val content: String,
    val attachments: List<Attachment>?
) {

    data class Attachment(
        val id: String,
        val title: String,
        val url: String,
        val thumbnailUrl: String
    )
}
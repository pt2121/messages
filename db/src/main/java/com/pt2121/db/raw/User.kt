package com.pt2121.db.raw

/**
 * represents User in data.json
 */
internal data class User(
    val id: Long,
    val name: String,
    val avatarId: String
)